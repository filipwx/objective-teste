async function fetchAsync(url) {
    let response = await fetch(url);
    let data = await response.json();
    return data;
}

async function listCharacters(url = null, search = false) {
    try {
        let data
        if (url == null) {
            data = await fetchAsync('https://kitsu.io/api/edge/anime-characters');
        } else {
            data = await fetchAsync(url);
        }
        const array_return = data.data;
        if (array_return.length == 0) {
            listCharacters();
        }
        data = []

        if (search) {
            const listHtml = document.getElementById("listItens");
            listHtml.innerHTML = "";

            array_return.map(elem => {
                const iten = document.createElement("li");
                iten.setAttribute("id", elem.id);
                const titulo = document.createElement("h5");
                const text = document.createTextNode(String(elem.attributes.canonicalName));
                titulo.appendChild(text);
                const subtitulo = document.createElement("p");
                const description = document.createTextNode(elem.attributes.description);
                subtitulo.appendChild(description);
                const div = document.createElement("div");
                div.appendChild(titulo);
                div.appendChild(subtitulo);
                const image = document.createElement("img");
                image.setAttribute("src", elem.attributes.image.original);
                iten.appendChild(image);
                iten.appendChild(div);
                listHtml.appendChild(iten);
            });
            document.getElementById('listConters').style.opacity = 0;
        } else {
            document.getElementById('listConters').style.opacity = 1;
            const promises = array_return.map(async item => {
                let data_returned = await fetchAsync(item.relationships.character.links.related);
                data.push(data_returned.data);
                return data_returned.data;
            });

            await Promise.all(promises);

            const listHtml = document.getElementById("listItens");
            listHtml.innerHTML = "";

            data.map(elem => {
                const iten = document.createElement("li");
                iten.setAttribute("id", elem.id);
                iten.setAttribute("onclick", `getdetails(${elem.id}); return false;`)
                const titulo = document.createElement("h5");
                const text = document.createTextNode(String(elem.attributes.canonicalName));
                titulo.appendChild(text);
                const subtitulo = document.createElement("p");
                const description = document.createTextNode(elem.attributes.description);
                subtitulo.appendChild(description);
                const div = document.createElement("div");
                div.appendChild(titulo);
                div.appendChild(subtitulo);
                const image = document.createElement("img");
                image.setAttribute("src", elem.attributes.image.original);
                iten.appendChild(image);
                iten.appendChild(div);
                listHtml.appendChild(iten);
            });
        }
    } catch (error) {
        console.log('Error: ', error);
    }
}
listCharacters();
function getit(id) {
    listCharacters(`https://kitsu.io/api/edge/anime-characters?page%5Blimit%5D=10&page%5Boffset%5D=${id * 10}`);
}
function getdetails(id) {
    console.log('Chegou o ID: ', id);
    localStorage.setItem('kitsu', id);
    window.location.href = '/detalhes';
    // listCharacters(`https://kitsu.io/api/edge/anime-characters?page%5Blimit%5D=10&page%5Boffset%5D=${id * 10}`);
}
function setPaginate() {
    const listConters = document.getElementById("listConters");
    for (let index = 1; index < 10; index++) {
        const iten = document.createElement("li");
        const span = document.createElement("span");
        const text = document.createTextNode(index);
        span.setAttribute("onclick", `getit(${index}); return false;`)
        span.setAttribute("id", index)
        span.appendChild(text);
        iten.appendChild(span);
        listConters.appendChild(iten);
    }
}
setPaginate();
var userDateEntry = document.getElementById("personagem");
try {
    userDateEntry.addEventListener('change',
        function () {
            if (userDateEntry.value != null || userDateEntry.value != '') {
                console.log('Input: ', userDateEntry.value);
                let url = `https://kitsu.io/api/edge/characters?filter%5Bname%5D=${userDateEntry.value}`;
                listCharacters(url, search = true);
            } else {
                listCharacters();
            }
        }
    );
} catch (error) {
    console.log('Error: ', error);
}