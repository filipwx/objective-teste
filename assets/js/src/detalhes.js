async function fetchAsync(url) {
    let response = await fetch(url);
    let data = await response.json();
    return data;
}
async function loadMedia(params) {
    try {
        const retorno = await fetchAsync(params);
        let data = []

        const promises = retorno.data.map(async item => {
            let data_returned = await fetchAsync(item.relationships.media.links.related);
            data.push(data_returned.data);
            return data_returned.data;
        });
        await Promise.all(promises);

        const listAnimes = document.getElementById("listAnimes");
        listAnimes.innerHTML = "";

        data.map( elem => {
            const iten = document.createElement("li");
            iten.setAttribute("id", elem.id);
            const titulo = document.createElement("h5");
            const text = document.createTextNode(String(elem.attributes.canonicalTitle));
            titulo.appendChild(text);

            const image = document.createElement("img");
            image.setAttribute("src", elem.attributes.posterImage.medium);
            iten.appendChild(image);
            iten.appendChild(titulo);
            listAnimes.appendChild(iten);
        });


    } catch (error) {
        
    }
}
async function loadCharacter() {
    let kitsu;
    try {
        kitsu = localStorage.getItem('kitsu');
    } catch (error) {
        kitsu = null;
    }
    if (kitsu !== null && kitsu !== 0 && kitsu !== '' && kitsu !== undefined) {
        try {
            const retorno = await fetchAsync(`https://kitsu.io/api/edge/characters/${kitsu}`);
            console.log('Retunr: ', retorno.data);

            const itenHtml = document.getElementById("personItens");
            itenHtml.innerHTML = "";

            const titulo = document.createElement("h4");
            const text = document.createTextNode(String(retorno.data.attributes.canonicalName));
            titulo.appendChild(text);
            const subtitulo = document.createElement("p");
            const description = document.createTextNode(retorno.data.attributes.description);
            subtitulo.appendChild(description);
            const div = document.createElement("div");

            div.appendChild(titulo);
            if (retorno.data.attributes.names.ja_jp) {
                const japa_name = document.createElement("p");
                const japa_name_name = document.createTextNode(retorno.data.attributes.names.ja_jp);
                japa_name.appendChild(japa_name_name);
                div.appendChild(japa_name);
            }

            div.appendChild(subtitulo);

            const image = document.createElement("img");
            image.setAttribute("src", retorno.data.attributes.image.original);
            itenHtml.appendChild(image);
            itenHtml.appendChild(div);
            loadMedia(`https://kitsu.io/api/edge/characters/${kitsu}/media-characters`)
        } catch (error) {
            console.log('Errado: ', error)
        }
    }
}
var userDateEntry = document.getElementById("voltar");
try {
    userDateEntry.addEventListener('click',
        function () {
            localStorage.setItem('kitsu', null);
            window.location.href = '/';
        }
    );
} catch (error) {
    console.log('Error: ', error);
}